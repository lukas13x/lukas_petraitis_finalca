import System.Random (randomRIO)
--main app
main :: IO ()
main = do 
    putStrLn "Do you want to play 1) Easy, 2) Medium or 3) Hard"
    putStrLn "Select your difficulty by entering a number 1, 2 or 3"
    choice <- readLn -- reads user input
    if (choice == 1) 
    then do
     putStrLn "You selected easy"
     easy -- if the choice is 1, it goes to function easy
    else if (choice == 2)
    then do
     putStrLn "You selected medium"
     medium -- if the choice is 2, it goes to function medium
    else if (choice == 3)
    then do 
     putStrLn "You selected hard"
     hard -- if the choice is 3, it goes to the function hard
    else do
     putStrLn "You did not select a valid choice"
     main

easy :: IO ()
easy = do
         putStrLn "Guess a number between 1 and 10: "
         rNumber <- randomRIO (1, 10) :: IO Int -- creates a random number between 1 and 10
         loop rNumber 3 --goes to the loop function

medium :: IO ()
medium = do
         putStrLn "Guess a number between 1 and 25: " -- between 1 and 25
         rNumber <- randomRIO (1, 25) :: IO Int
         loop rNumber 5 --goes to the loop function

hard :: IO ()
hard = do
         putStrLn "Guess a number between 1 and 50: " -- between 1 and 50
         rNumber <- randomRIO (1, 50) :: IO Int
         loop rNumber 10 --goes to the loop function

loop :: Int -> Int -> IO ()
loop rNumber rGuesses = do -- does this function until the correct answer is selected
    putStrLn $ "You have " ++ show rGuesses ++ " guesses remaining"
    userInput <- readLn
    if userInput < rNumber  -- if the unput is lower than the random number
    then do
     putStrLn "Too low; guess again: " 
     loop rNumber (rGuesses - 1) -- calls function again and adds to the number of guesses
    else if userInput > rNumber -- if the input is higher than the random number
    then do
     putStrLn "Too high; guess again: "
     loop rNumber (rGuesses - 1)
    else if rGuesses == 0 
    then do
     putStrLn "You used up all of your guesses, Play again !"
     main 
     else do
     putStrLn $ "Correct!"
     main

--data suits = Heart | Spade | Club | Diamond deriving (Show)
--data cards = Two | Three | Four | Five | Six | Seven | Eight | Nine | Ten | Jack | Queen | King | Ace deriving (Eq, Ord, Show, Read, Bounded, Enum)
--instance Random Weapon where
    --random g = case randomR (0,2) g of
                 --(r, g') -> (toEnum r, g')
    --randomR (a,b) g = case randomR (fromEnum a, fromEnum b) g of
                        --(r, g') -> (toEnum r, g')